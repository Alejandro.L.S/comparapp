<?php
$nom=$_GET['nom'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="style.css">

    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">

    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>ComparApp</title>
  </head>
  <body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
  <a class="navbar-brand" href="#">Bienvenido <?php echo $nom ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
    	<li class="nav-item">
        <a class="nav-link" href="promociones.php">Mis Promociones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="administrador/salir.php">Salir</a>
      </li>
    </ul>
   
  </div>
</div>
</nav>


<section id="hero">
<div class="container">
  <div class="content-center topmargin-lg">
  <h1></h1>
 
</div>
</div>
</section>


  </body>
</html>

