<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->image('imagenes/descarga.png',85,0,30);

    $this->Ln(10);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,10,'Reportes Supermercados',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    
    $this->cell(5,10,'Id',1,0,'c',0);
    $this->cell(20,10,'Cuit',1,0,'c',0);
    
    $this->cell(40,10,'Nom',1,0,'c',0);
    $this->cell(40,10,'Dom',1,0,'c',0);
    $this->cell(25,10,'Tel',1,0,'c',0);
    $this->cell(20,10,'Lat',1,0,'c',0);
    $this->cell(20,10,'Long',1,0,'c',0);
    $this->cell(30,10,'F.ing',1,1,'c',0);
    
}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';

$consulta="SELECT * FROM supermarket_vista order by idsupermarket";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

    $pdf->cell(5,8,$row['idsupermarket'],1,0,'c',0);
    $pdf->cell(20,8,$row['cuit'],1,0,'c',0);
    
    $pdf->cell(40,8,$row['namesupermarket'],1,0,'c',0);
    $pdf->cell(40,8,$row['address'],1,0,'c',0);
    $pdf->cell(25,8,$row['description'],1,0,'c',0);
    $pdf->cell(20,8,$row['latitude'],1,0,'c',0);
    $pdf->cell(20,8,$row['longitude'],1,0,'c',0);
    $pdf->cell(30,8,$row['datetime'],1,1,'c',0);

   
}

$pdf->Output();
?>