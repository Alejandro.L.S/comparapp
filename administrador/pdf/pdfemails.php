<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->image('imagenes/descarga.png',95,0,30);

    $this->Ln(15);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(40);
    // Título
  $this->Cell(120,10,'Reportes Emails',0,0,'C');
    // Salto de línea
    $this->Ln(20);

  
    $this->cell(5,10,'Id',1,0,'c',0);
    $this->cell(24,10,'Nombre',1,0,'c',0);
    $this->cell(40,10,'Email',1,0,'c',0);
    $this->cell(104,10,'Experiencia',1,0,'c',0);
    $this->cell(38,10,utf8_decode('Fecha.in'),1,1,'c',0);

}
// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';
$recibo1=$_GET['id'];
$consulta="SELECT * FROM  emails  WHERE id = '$recibo1'";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

   $pdf->cell(5,8,$row['id'],1,0,'c',0);
    $pdf->cell(24,8,$row['name'],1,0,'c',0);
    $pdf->cell(40,8,$row['email'],1,0,'c',0);
    $pdf->cell(104,8,$row['experience'],1,0,'c',0);
    $pdf->cell(38,8,$row['datatime'],1,1,'c',0);
  
}

$pdf->Output();

?>