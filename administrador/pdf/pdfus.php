<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->image('imagenes/descarga.png',85,0,30);

    $this->Ln(10);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,10,'Reportes Usuarios',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    
     $this->cell(5,10,'Id',1,0,'c',0);
    $this->cell(12,10,'T.dni',1,0,'c',0);
    $this->cell(15,10,'N.dni',1,0,'c',0);
    $this->cell(19,10,'Nom',1,0,'c',0);
    $this->cell(19,10,'ape',1,0,'c',0);
    $this->cell(17,10,'F.nac',1,0,'c',0);
    $this->cell(15,10,'Sexo',1,0,'c',0);
    $this->cell(48,10,'Email',1,0,'c',0);
    $this->cell(40,10,'Cont',1,0,'c',0);
    $this->cell(12,10,'Rol',1,1,'c',0);
   
}



// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';

$recibo1=$_GET['id'];
$consulta="SELECT * FROM  Users_vista  WHERE id_user = '$recibo1'";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

    $pdf->cell(5,8,$row['id_user'],1,0,'c',0);
    $pdf->cell(12,8,$row['description'],1,0,'c',0);
    $pdf->cell(15,8,$row['document_number'],1,0,'c',0);
    $pdf->cell(19,8,$row['name'],1,0,'c',0);
    $pdf->cell(19,8,$row['surname'],1,0,'c',0);
    $pdf->cell(17,8,$row['date_birth'],1,0,'c',0);
    $pdf->cell(15,8,$row['sexdescription'],1,0,'c',0);
    $pdf->cell(48,8,$row['email'],1,0,'c',0);
    $pdf->cell(40,8,$row['password'],1,0,'c',0);
    $pdf->cell(12,8,$row['roldescription'],1,1,'c',0);
    
  }

$pdf->Output();
?>