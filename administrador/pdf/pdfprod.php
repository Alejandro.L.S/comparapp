<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->image('imagenes/descarga.png',85,0,30);

    $this->Ln(10);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,10,'Reportes Productos',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    
    
    $this->cell(8,10,'Id',1,0,'c',0);
    $this->cell(20,10,'Estado',1,0,'c',0);
    $this->cell(20,10,'Region',1,0,'c',0);
    $this->cell(32,10,'Categoria',1,0,'c',0);
    $this->cell(28,10,'Sub.Cat',1,0,'c',0);
    $this->cell(18,10,'Marca',1,0,'c',0);
   
    $this->cell(50,10,'Descripcion',1,0,'c',0);
    $this->cell(25,10,'Cod.Barra',1,1,'c',0);
    
   
}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';

$recibo1=$_GET['id'];
$consulta="SELECT * FROM  products_vista  WHERE idproduct = '$recibo1'";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

    $pdf->cell(8,8,$row['idproduct'],1,0,'c',0);
    $pdf->cell(20,8,$row['estate_description'],1,0,'c',0);
    $pdf->cell(20,8,$row['region_description'],1,0,'c',0);
    $pdf->cell(32,8,$row['category_description'],1,0,'c',0);
    $pdf->cell(28,8,$row['sub_description'],1,0,'c',0);
    $pdf->cell(18,8,$row['brand_description'],1,0,'c',0);
   
    $pdf->cell(50,8,$row['description'],1,0,'c',0);
    $pdf->cell(25,8,$row['barcode'],1,1,'c',0);
}

$pdf->Output();
?>