
<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->image('imagenes/descarga.png',85,0,30);

    $this->Ln(10);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,10,'Lista ',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    
    $this->cell(40,10,'Supermercado',1,0,'c',0);
    $this->cell(80,10,'Domicilio',1,0,'c',0);
     $this->cell(50,10,'Productos',1,0,'c',0);
      $this->cell(15,10,'Cant Prodl',1,0,'c',0);
    $this->cell(20,10,'Total',1,1,'c',0);
    
}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';

$consulta="SELECT * FROM lista ";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

      $pdf->cell(40,8,$row['super'],1,0,'c',0);
    $pdf->cell(80,8,$row['domicilio'],1,0,'c',0);
     $pdf->cell(50,8,$row['productos'],1,0,'c',0);
      $pdf->cell(15,8,$row['cantidad'],1,0,'c',0);
    $pdf->cell(20,8,$row['total'],1,1,'c',0);
   
}

$pdf->Output();
?>

