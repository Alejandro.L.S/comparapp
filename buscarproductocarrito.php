

<?php
$servidor="localhost";
$user="vamdev_comparapp";
$pass="alejandrolscai";
$database_1="vamdev_comparapp";
$link = mysqli_connect($servidor,$user,$pass,$database_1);
$tildes = $link->query("SET NAMES 'utf8'"); //Para que se muestren las tildes
// Deben colocar el nombre de la tabla de su base, por ejemplo el de personas para ver algun dato
$resultado = mysqli_query($link," truncate lista");
// cierra la conexión
mysqli_close($link);
?>
<!DOCTYPE html>
<html lang="en">

<head><meta charset="gb18030">
    
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="carpetaparacarrito/css/bootstrap.min.css">
   
    <script src="js/popper.min.js"></script>
   
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <link rel="stylesheet" href="carpetaparacarrito/css/sweetalert2.min.css">

    <title>ComparApp</title>
</head>

<body  >


    <header>
        <div class="container">
            <div class="row align-items-stretch justify-content-between">
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <a class="navbar-brand" href="#">ComparApp</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <img src="imagenes/cart.jpeg" class="nav-link dropdown-toggle img-fluid" height="70px"
                                    width="70px" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"></img>
                             
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <main>

        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 my-4 mx-auto text-center">
            <h1 class="display-4 mt-4">Lista de Productos</h1>
            <p class="lead" >Selecciona nuestros productos y comienza a comparar</p>
        </div>
<center>


<section class="principal">


    <div class="formulario" id="lista-productos" >
        <label for="caja_busqueda">Buscar</label>
        <input type="text" name="caja_busqueda" id="caja_busqueda"></input>
        

<a href="fin.php"class="btn btn-success " role="button">Finalizar </a>
<a href="comparar.php"class="btn btn-danger " role="button">Comparar </a>


        
<br><br>
        
    </div>
    <div id="datos"></div>

</section>



</center>
    </main>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
    <script src="carpetacarrito/js/jquery-3.4.1.min.js"></script>
    <script src="carpetacarrito/js/bootstrap.min.js"></script>
    <script src="carpetacarrito/js/sweetalert2.min.js"></script>
    <script src="carpetacarrito/js/carrito.js"></script>
    <script src="carpetacarrito/js/js/pedido.js"></script>
    

</body>

</html>

