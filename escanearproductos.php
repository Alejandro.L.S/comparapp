
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="carpetaparacarrito/css/bootstrap.min.css">
    <link rel="stylesheet" href="carpetaparacarrito/css/estilo.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="carpetaparacarrito/css/sweetalert2.min.css">
	<script type="text/javascript"  src="js/jquery.js"></script>
	<script type="text/javascript"  src="js/barcode.js"></script>
	<title>ComparApp Barcode</title>
	<center>

	<script type="text/javascript">
		 
		 var sound = new Audio("sonido/barcode.wav");

		 $(document).ready(function(){

         barcode.config.start = 0.2;

         barcode.config.end = 0.9;

         barcode.config.video = '#barcodevideo';

         barcode.config.canvas = '#barcodecanvas';

         barcode.config.canvasg = '#barcodecanvasg';

         barcode.setHandler(function(barcode){

         $('#result').html(barcode);


         })

         barcode.init();

         $('#result').bind('DOMSubtreeModified',function(e){

         	sound.play();
         });

		 }); 

	</script>
</head>
<?php
session_start();
?>
<body>

     <header>
        <div class="container">
            <div class="row align-items-stretch justify-content-between">
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <a class="navbar-brand" href="#">ComparApp</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <img src="imagenes/cart.jpeg" class="nav-link dropdown-toggle img-fluid" height="70px"
                                    width="70px" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"></img>
                             
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

    </header>



    <main>
       

	<div id="barcode">
    <video id="barcodevideo" autoplay></video>	
    <canvas id="barcodecanvasg"></canvas>	
	</div>
	<canvas id="barcodecanvas"></canvas>
	<div id="result"></div>
	<br><br><br><br><br>
	<form method="POST" action="barcode.php" >
    <div class="form-group">
        <label for="nombre">INGRESAR MANUELMENTE</label><br>
        <input width="20%" type="text" name="bar" class="" id="bar" value="" placeholder="Ingrese el còdigo">
        <input type="submit" class ='btn btn-primary' value="Enviar">
       <a href="index.html"><button type='button' class='btn btn-primary'>Finalizar</button></a>
       
    </div>
</form>
</center>
</body>
</html>